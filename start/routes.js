'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Route = use('Route')

Route.group(() => { 
    Route.get('/', 'UsuarioController.index');
    Route.post('/alumno', 'UsuarioController.storeAlumno');
    Route.post('/profesor', 'UsuarioController.storeProfesor');
    Route.post('/login', 'UsuarioController.login');
    Route.get('/logout', 'UsuarioController.logout').middleware(['auth']).as('logout');
    Route.put('/reset_contrasenia', 'UsuarioController.resetContrasenia');
}).prefix('/usuarios');

Route.group(() => { 
    Route.get('/', 'AlumnoController.index').as('alumno.index');
    Route.get('/materia/:id', 'MateriaController.detalleMateria').formats(['json']);
    Route.get('/examen/:id', 'ExamenController.realizar').formats(['json']);
    Route.get('/materias', 'MateriaController.materiasInscrito').as('alumno.materias').formats(['json']);
    Route.put('/examen', 'ExamenController.calificacion')
}).prefix('/alumno').middleware(['auth', 'authorization:alumno']);

Route.group(() => { 
    // ALUMNO
    Route.get('/', 'MateriaController.indexAlumno').middleware(['auth', 'authorization:alumno' ]);
    Route.post('/inscribir', 'MateriaController.inscribirAlumno').middleware(['auth', 'authorization:alumno']);
    // PROFESOR
    Route.put('/solicitud', 'MateriaController.solicitud').middleware(['auth', 'authorization:profesor']);
    Route.get('/:id/examen', 'MateriaController.crearExamen').middleware(['auth', 'authorization:profesor']);
    Route.post('/examen', 'MateriaController.guardarExamen').middleware(['auth', 'authorization:profesor']);
    Route.delete('/:id', 'MateriaController.eliminar').middleware(['auth', 'authorization:profesor']);
    Route.put('/', 'MateriaController.actualizar').middleware(['auth', 'authorization:profesor']);
    Route.delete('/examen/:id', 'MateriaController.eliminarExamen').middleware(['auth', 'authorization:profesor']);
}).prefix('/materia')

Route.group(() => { 
    Route.get('/', 'ProfesorController.index').as('profesor.index')
    Route.get('/materias', 'MateriaController.indexProfesor').as('profesor.materias').formats(['json']);
    Route.get('/materia/:id', 'MateriaController.gestionarMateria').formats(['json'])
    Route.post('/materia', 'MateriaController.store');
    
    // PDF's
    Route.get('/:tipo/:id', 'ExamenController.generarReporte');
}).prefix('/profesor').middleware(['auth', 'authorization:profesor']);

Route.get('file/:name', 'MateriaController.getImage');