const Event = use('Event')
const Mail = use('Mail')
const Env = use('Env')

Event.on('notificarProfesor::usuario', async (data) => {
    console.log('email send to => ',data.materia.profesor.usuario.correo);
    const message = `
        Sistema de gestión de examenes \n
        ${data.materia.profesor.nombre} ${data.materia.profesor.ap_paterno} ${data.materia.profesor.ap_materno} \n
        Ha recibido una solicitud de inscripción para la materia: ${data.materia.clave} ${data.materia.nombre} \n
        De parte del alumno : ${data.alumno.nombre} ${data.alumno.ap_paterno} ${data.alumno.ap_materno} \n
        Por favor ingrese al sistema de gestión de examenes para aprobar la solicitud.
        localhost:${Env.get('PORT', '')}/profesor/materia/${data.materia.id}
    `
    await Mail.raw(message, (message) => {
            message.from('examenesitch@gmail.com')
            message.to(data.materia.profesor.usuario.correo);
    })
})

Event.on('resetContrasenia::usuario', async (data) => {
    console.log('email send to => ',data.correo);
    const message = `
        Sistema de gestión de examenes \n
        Se ha restaurado su contraseña.
        Su nueva contraseña es ${data.contrasenia_nva}
    `
    await Mail.raw(message, (message) => {
            message.from('examenesitch@gmail.com')
            message.to(data.correo);
    })
})