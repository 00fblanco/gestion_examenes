'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/Usuario', (faker, i, data) => {
  return {
    contrasenia: '123456',
    correo: data.correo,
    rol: data.rol
  }
})

Factory.blueprint('App/Models/Profesor', (faker, i, data) => {
    return {
      nombre: faker.last(),
      ap_paterno: faker.last(),
      ap_materno: faker.last(),
      sexo: 'm',
      usuario_id: data.usuario_id
    }
})

Factory.blueprint('App/Models/Alumno', (faker, i, data) => {
  return {
    no_control: data.no_control,
    nombre: faker.last(),
    ap_paterno: faker.last(),
    ap_materno: faker.last(),
    sexo: 'm',
    carrera: 'Ingeniería en sistemas computacionales',
    usuario_id: data.usuario_id
  }
})

Factory.blueprint('App/Models/Materia', (faker, i, data) => {
  return {
    clave: faker.integer({ min: 1, max: 1000 }),
    nombre: data.nombre,
    fecha_inicio: data.fecha_inicio,
    fecha_termino: data.fecha_termino,
    area: data.area,
    profesor_id: data.profesor_id
  }
})