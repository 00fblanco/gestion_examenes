'use strict'

const Schema = use('Schema')

class PreguntaSchema extends Schema {
  up () {
    this.create('preguntas', (table) => {
      table.increments()
      table.timestamps()

      table.text('descripcion', 'longtext').notNullable()
      table.enum('tipo', ['opc_mult', 'verdadero_falso', 'emparejamiento', 'relacion', 'resp_corta','arrastrar_soltar']).notNullable()
      table.integer('puntuacion').notNullable().defaultTo(1);
      table.text('retroalimentacion', 'mediumtext')
      // FK's

      table.integer('examen_id').unsigned().notNullable().references('id').inTable('examenes');
    })
  }

  down () {
    this.drop('preguntas')
  }
}

module.exports = PreguntaSchema
