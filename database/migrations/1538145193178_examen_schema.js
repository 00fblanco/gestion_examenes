'use strict'

const Schema = use('Schema')

class ExamenSchema extends Schema {
  up () {
    this.create('examenes', (table) => {
      table.increments()
      table.timestamps()
      table.string('nombre', 50).notNullable()
      table.text('descripcion', 'longtext')
      table.dateTime('fecha_aplicacion').notNullable()
      table.time('tiempo_aplicacion').notNullable()
      table.enum('tipo', ['prueba', 'final']).notNullable()
      //FK's

      table.integer('materia_id').unsigned().notNullable().references('id').inTable('materias');
    })
  }


  down () {
    this.drop('examenes')
  }
}

module.exports = ExamenSchema
