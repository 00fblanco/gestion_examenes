'use strict'

const Schema = use('Schema')

class MateriaSchema extends Schema {
  up () {
    this.create('materias', (table) => {
      table.increments()
      table.integer('clave').unsigned().notNullable()
      table.string('nombre', 50).notNullable()
      table.string('path_img', 150).defaultTo('/file/default.jpg')
      table.date('fecha_inicio').notNullable()
      table.date('fecha_termino').notNullable()
      table.string('area', 50).notNullable()
      table.timestamps()
      
      // FK's
      table.integer('profesor_id').unsigned().notNullable().references('id').inTable('profesores')
    })
  }

  down () {
    this.drop('materias')
  }
}

module.exports = MateriaSchema
