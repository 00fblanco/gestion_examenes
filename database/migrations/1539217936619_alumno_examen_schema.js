'use strict'

const Schema = use('Schema')

class AlumnoExamenSchema extends Schema {
  up () {
    this.create('alumno_examenes', (table) => {
      table.increments()
      table.timestamps()
      table.integer('calificacion').notNullable().defaultTo(0);
      // FK's
      table.integer('examen_id').unsigned().notNullable().references('id').inTable('examenes')
      table.integer('alumno_id').unsigned().notNullable().references('id').inTable('alumnos')
    })
  }

  down () {
    this.drop('alumno_examenes')
  }
}

module.exports = AlumnoExamenSchema
