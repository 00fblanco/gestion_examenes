'use strict'

const Schema = use('Schema')

class AlumnoMateriaSchema extends Schema {
  up () {
    this.create('alumno_materias', (table) => {
      table.increments()
      table.enum('estatus', ['solicitud', 'aceptado', 'rechazado']).notNullable().defaultTo('solicitud');
      table.timestamps()
      
      // FK's
      table.integer('alumno_id').unsigned().notNullable().references('id').inTable('alumnos');
      table.integer('materia_id').unsigned().notNullable().references('id').inTable('materias');

      // index
      table.unique(['alumno_id', 'materia_id'])
    })
  }

  down () {
    this.drop('alumno_materias')
  }
}

module.exports = AlumnoMateriaSchema
