'use strict'

const Schema = use('Schema')

class RespuestaSchema extends Schema {
  up () {
    this.create('respuestas', (table) => {
      table.increments()
      table.string('respuesta').notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('respuestas')
  }
}

module.exports = RespuestaSchema
