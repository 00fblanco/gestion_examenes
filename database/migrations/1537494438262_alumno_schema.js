'use strict'

const Schema = use('Schema')

class AlumnoSchema extends Schema {
  up () {
    this.create('alumnos', (table) => {
      table.increments()
      table.string('no_control', 8).notNullable().unique();
      table.string('nombre', 40).notNullable();
      table.string('ap_paterno', 40).notNullable();
      table.string('ap_materno', 40).notNullable();
      table.string('carrera', 50).notNullable()
      table.enu('sexo', ['m','f']);
      table.integer('usuario_id').unsigned().notNullable().references('id').inTable('usuarios');
      table.timestamps()
    })
  }

  down () {
    this.drop('alumnos')
  }
}

module.exports = AlumnoSchema
