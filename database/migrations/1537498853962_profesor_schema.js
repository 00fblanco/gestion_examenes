'use strict'

const Schema = use('Schema')

class ProfesorSchema extends Schema {
  up () {
    this.create('profesores', (table) => {
      table.increments()
      table.string('nombre', 40).notNullable();
      table.string('ap_paterno', 40).notNullable();
      table.string('ap_materno', 40).notNullable();
      table.enu('sexo', ['m','f']);
      table.timestamps()
      // FK's
      table.integer('usuario_id').unsigned().notNullable().references('id').inTable('usuarios');
      
    })
  }

  down () {
    this.drop('profesores')
  }
}

module.exports = ProfesorSchema
