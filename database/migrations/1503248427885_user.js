'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('usuarios', (table) => {
      table.increments()
      table.string('correo', 254).notNullable().unique()
      table.string('contrasenia', 60).notNullable()
      table.enu('rol', ['profesor', 'alumno']).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('usuarios')
  }
}

module.exports = UserSchema
