'use strict'

const Schema = use('Schema')

class PreguntaRespuestasSchema extends Schema {
  up () {
    this.create('pregunta_respuestas', (table) => {
      table.increments()
      table.timestamps()
      table.enum('esCorrecta', ['si', 'no']).defaultTo('no');
      table.integer('pregunta_id').unsigned().notNullable().references('id').inTable('preguntas');
      table.integer('respuesta_id').unsigned().notNullable().references('id').inTable('respuestas');
    })
  }

  down () {
    this.drop('pregunta_respuestas')
  }
}

module.exports = PreguntaRespuestasSchema
