'use strict'

/*
|--------------------------------------------------------------------------
| InitSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class InitSeeder {
  async run () {
    const usuario = await Factory.model('App/Models/Usuario').create({correo: 'canto@dev.com', rol: 'profesor'})
    const profesor = await Factory.model('App/Models/Profesor').create({ usuario_id: usuario.id });

    const usuario2 = await Factory.model('App/Models/Usuario').create({correo: 'yanet@dev.com', rol: 'profesor'})
    const profesor2 = await Factory.model('App/Models/Profesor').create({ usuario_id: usuario2.id });

    // alumnos
    const usuario3 = await Factory.model('App/Models/Usuario').create({correo: 'fb@dev.com', rol: 'alumno'})
    const alumno3 = await Factory.model('App/Models/Alumno').create({ usuario_id: usuario3.id, no_control:13520424 });

    const usuario4 = await Factory.model('App/Models/Usuario').create({correo: 'jose@dev.com', rol: 'alumno'})
    const alumno4 = await Factory.model('App/Models/Alumno').create({ usuario_id: usuario4.id , no_control: 12234251});

    // Materias
    const materia = await Factory.model('App/Models/Materia').create({nombre: 'Automatas', fecha_inicio: '2018-09-24', fecha_termino: '2018-08-30', area: 'Ciencias basicas', profesor_id: profesor.id});
    const materia2 = await Factory.model('App/Models/Materia').create({nombre: 'Automatas II', fecha_inicio: '2018-09-24', fecha_termino: '2018-08-30', area: 'Ciencias basicas', profesor_id: profesor.id});
  }
}

module.exports = InitSeeder
