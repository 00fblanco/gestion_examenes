'use strict'

class Authorization {
  async handle ({ request, auth, response }, next, schemes) {
    const rol = schemes[0];
    if(rol === auth.user.rol){
      return await next();
    }
    return response.status(203).json({messages: `No esta autorizado para este recurso, necesita autorización de tipo [${rol}]`})
  }
}

module.exports = Authorization
