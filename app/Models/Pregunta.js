'use strict'

const Model = use('Model')

class Pregunta extends Model {

    static get table(){
        return 'preguntas';
    }

    examen(){ 
        return this.belongsTo('App/Models/Examen')
    }

    respuestas(){
        return this.hasMany('App/Models/PreguntaRespuesta');
    }
}

module.exports = Pregunta
