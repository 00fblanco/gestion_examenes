'use strict'

const Model = use('Model')

class Respuesta extends Model {

    preguntas(){ 
        return this.hasMany('App/Models/PreguntaRespuesta')
    }
}

module.exports = Respuesta
