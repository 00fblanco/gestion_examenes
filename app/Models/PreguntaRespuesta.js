'use strict'

const Model = use('Model')

class PreguntaRespuesta extends Model {


    pregunta(){
        return this.belongsTo('App/Models/Pregunta')
    }
    respuesta(){
        return this.belongsTo('App/Models/Respuesta')
    }
}

module.exports = PreguntaRespuesta
