'use strict'

const Model = use('Model')

class Profesor extends Model {

    static get table(){
        return 'profesores';
    }

    materias(){
        return this.hasMany('App/Models/Materia')
    }
    usuario(){
        return this.belongsTo('App/Models/Usuario');
    }    
}

module.exports = Profesor
