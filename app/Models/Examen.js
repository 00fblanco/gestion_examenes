'use strict'

const Model = use('Model')

class Examen extends Model {

    static get table(){
        return 'examenes';
    }

    materia(){ 
        return this.belongsTo('App/Models/Materia')
    }

    preguntas(){
        return this.hasMany('App/Models/Pregunta', 'id', 'examen_id')
    }
    alumnos(){
        return this.hasMany('App/Models/AlumnoExamen', 'id', 'examen_id');
    }
}

module.exports = Examen
