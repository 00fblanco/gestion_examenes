'use strict'

const Model = use('Model')

class AlumnoExamen extends Model {
    static get table(){
        return 'alumno_examenes'
    }

    alumno (){ 
        return this.belongsTo('App/Models/Alumno', 'alumno_id', 'id')
    }
    examen(){
        return this.belongsTo('App/Models/Examen', 'examen_id', 'id')
    }
}

module.exports = AlumnoExamen
