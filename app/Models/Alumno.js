'use strict'

const Model = use('Model')

class Alumno extends Model {


    static get table(){
        return 'alumnos';
    }
    static get computed () {
        return ['nombre_completo']
    }

    getNombreCompleto({ nombre, ap_paterno, ap_materno }){
        return `${nombre} ${ap_paterno} ${ap_materno}`;
    }

    examenes(){
        return this.hasMany('App/Models/AlumnoExamen', 'id', 'alumno_id');
    }
}

module.exports = Alumno
