'use strict'

const Hash = use('Hash')
const Model = use('Model')

class Usuario extends Model {
  static getTable(){
    return 'usuarios';
  }
  static get hidden(){
    return ['contrasenia'];
  }

  alumno(){
    return this.belongsTo('App/Models/Alumno');
  }

  profesor(){
    return this.belongsTo('App/Models/Profesor');
  }

  static boot () {
    super.boot()
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.contrasenia) {
        userInstance.contrasenia = await Hash.make(userInstance.contrasenia)
      }
    })
  }
}

module.exports = Usuario
