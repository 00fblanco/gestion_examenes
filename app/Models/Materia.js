'use strict'

const Model = use('Model')
const Drive = use('Drive')

class Materia extends Model {

    static get table(){
        return 'materias';
    }

    profesor(){
        return this.belongsTo('App/Models/Profesor')
    }

    alumnos(){
        return this.hasMany('App/Models/AlumnoMateria')
    }
    examenes(){
        return this.hasMany('App/Models/Examen')
    }
}

module.exports = Materia
