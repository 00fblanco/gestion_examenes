'use strict'

const Model = use('Model')

class AlumnoMateria extends Model {
    static get table(){
        return 'alumno_materias'
    }
    alumno(){
        return this.belongsTo('App/Models/Alumno');
    }
    materia(){
        return this.belongsTo('App/Models/Materia')
    }
}

module.exports = AlumnoMateria
