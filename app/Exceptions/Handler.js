'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {

  async handle (error, { request, response }) {
    console.log('->HANDLER', error.name)
    if(error.name === 'ExpiredJwtToken'){
      return response.status(error.status).json({message: 'JWT ha expirado, vuelva a autenticarse'});
    }else if(error.name === 'ModelNotFoundException'){
      return response.status(203).json({ message: 'Recurso no encontrado'})
    }else if(error.name === 'InvalidSessionException'){
      return response.redirect('/usuarios/')
    }else if(error.name === 'ER_DUP_ENTRY'){
      return response.status(203).json({message: 'No se puede duplicar el registro.'});
    }
    
    return response.status(error.status).json({ message: error.message})
  }

  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
