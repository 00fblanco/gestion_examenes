'use strict'
const Materia = use('App/Models/Materia');
const AlumnoMateria = use('App/Models/AlumnoMateria')
const Examen = use('App/Models/Examen')
const Pregunta = use('App/Models/Pregunta');
const Respuesta = use('App/Models/Respuesta');
const PreguntaRespuesta = use('App/Models/PreguntaRespuesta');
const Helpers = use('Helpers')
const Event = use('Event')
const Database = use('Database');
const moment = use('moment')
class MateriaController {
    async eliminarExamen({ request, response, session }){
        const examen_id = request.params.id;
        const affectedRows = await Examen
            .query()
            .where('id', examen_id)
            .delete()
        //console.log(affectedRows)
        return response.status(200).json({ messsage: 'Eliminado satisfactoriamente'})
    }
    async eliminar({ request, response, session }){
        const materia_id = request.params.id;
        const profesor_id = session.get('id_profesor');

        const affectedRows = await Materia
            .query()
            .where('id', materia_id)
            .where('profesor_id', profesor_id)
            .delete()
        //console.log(affectedRows)
        return response.status(200).json({ messsage: 'Eliminado satisfactoriamente'})
    }

    async actualizar({ request, response, session }){
        const data = request.only(['materia_id','nombre', 'fecha_inicio', 'fecha_termino', 'area' ]);
        const profesor_id = session.get('id_profesor');
        //console.log(data)
        const materia = await Materia
            .query()
            .where('id', data.materia_id)
            .where('profesor_id', profesor_id)
            .firstOrFail()
        materia.merge({
                nombre: data.nombre,
                fecha_inicio: data.fecha_inicio,
                fecha_termino: data.fecha_termino,
                area: data.area
            })
        await materia.save()
        return response.status(200).json({ messsage: 'Actualziado satisfactoriamente'})

    }
    async detalleMateria({ request, session, view, response }){
        const materia_id = request.params.id;
        const alumno_id = session.get('id_alumno');
        if(request.format() === 'json'){
            const alumnoMateria = await AlumnoMateria.query()
            .where('materia_id', materia_id )
            .where('alumno_id', alumno_id)
            .where('estatus', 'aceptado')
            .with('materia', builder => {
                builder.with('examenes', bui => {
                    bui.with('alumnos', b => {
                        b.where('alumno_id', alumno_id);
                    })
                })
                builder.with('profesor')
            })
            .firstOrFail()
            
            return response.status(200).json(alumnoMateria)
        }
        
        return view.render('alumno.materia.index', { id: materia_id })
    }   
    async guardarExamen({ request, response }){
        const data = request.only(['materia_id', 'nombre', 'descripcion', 'fecha_aplicacion', 'tiempo_aplicacion', 'tipo', 'preguntas', 'respuestas_opcion', 'respuestas_relacion', 'respuestas_emparejamiento']);
        //console.log(data);
        if(data.materia_id === 0){
            return response.status(203).json({message: 'Error en su solicitud'})
        }
        // //const trx = await Database.beginTransaction()
        const examen = await Examen.create({
            materia_id: data.materia_id,
            nombre: data.nombre,
            descripcion: data.descripcion,
            tipo: data.tipo,
            fecha_aplicacion: data.fecha_aplicacion,
            tiempo_aplicacion: data.tiempo_aplicacion
        })
        
        await data.preguntas.map(async _pregunta => {
            const pregunta = await Pregunta.create({ examen_id: examen.id, descripcion: _pregunta.descripcion, tipo: _pregunta.tipo, puntuacion: _pregunta.puntuacion, retroalimentacion: _pregunta.retroalimentacion })
            if(pregunta.tipo === 'opc_mult' || pregunta.tipo === 'verdadero_falso' || pregunta.tipo === 'resp_corta' || pregunta.tipo === 'arrastrar_soltar'){
                //console.log(`resp ${resp.pregunta_id} == _preg ${_pregunta.pregunta_id} = ${resp.pregunta_id === _pregunta.pregunta_id}`);
                let resp_aguardar = await data.respuestas_opcion.filter(  resp =>  resp.pregunta_id === _pregunta.pregunta_id)
                await resp_aguardar.map( async _respuesta => {
                    //console.log(`tipo: ${pregunta.tipo}, respuesta => ${_respuesta.respuesta}`)
                    const respuesta = await Respuesta.create({ respuesta: _respuesta.respuesta })
                    await PreguntaRespuesta.create({ esCorrecta: _respuesta.esCorrecta, pregunta_id: pregunta.id, respuesta_id: respuesta.id })
                })
                //console.log("------------",resp_saving)
            }else if(pregunta.tipo === 'emparejamiento'){
                await data.respuestas_emparejamiento.map(async _respuesta => {
                    const respuesta = await Respuesta.create({ respuesta: _respuesta.respuesta });
                    await PreguntaRespuesta.create({ esCorrecta: _pregunta.pregunta_id === _respuesta.pregunta_id ? 'si' : 'no', pregunta_id: pregunta.id, respuesta_id: respuesta.id })
                })
            }else if(pregunta.tipo === 'relacion'){
                let resp_aguardar = await data.respuestas_relacion.filter(  resp =>  resp.pregunta_id === _pregunta.pregunta_id)
                await resp_aguardar.map( async _respuesta => {
                    //console.log(`tipo: ${pregunta.tipo}, respuesta => ${_respuesta.respuesta}`)
                    const respuesta = await Respuesta.create({ respuesta: _respuesta.respuesta })
                    await PreguntaRespuesta.create({ esCorrecta: _respuesta.esCorrecta, pregunta_id: pregunta.id, respuesta_id: respuesta.id })
                })
            }
        })

        return response.status(200).json({ message: 'Examen creado correctamente' });
        

    }
    async crearExamen({ request, response, view }){
        const id = request.params.id;

        return view.render('profesor.materia.crearExamen', { id })
    }
    async solicitud({ request, response, session }){
        const data = request.only(['alumno_id', 'materia_id', 'estatus']);
        const profesor_id = session.get('id_profesor');
        const affectedRows = await AlumnoMateria
            .query()
            .where('alumno_id', data.alumno_id)
            .where('materia_id', data.materia_id)
            .update({
                estatus: data.estatus
            })
             // validar que la materia sea del profesor
        if(affectedRows > 0){
            return response.status(200).json({ message: 'Actualización correcta'});
        }
        return response.status(203).json({message: 'Error: intente mas tarde.'})
    }
    async getImage({ params, response }){
        const fileName = params.name
        response.download(`tmp/uploads/${fileName}`)
    }
    async gestionarMateria({ request, params, view, session, response }){
        const id = params.id;
        const id_profesor = session.get('id_profesor')
        
        if(request.format() === 'json'){
            const materia = await Materia.query()
            .where('id', id)
            .where('profesor_id', id_profesor)
            .with('profesor')
            .with('alumnos', builder => {
                builder.with('alumno', bui => {
                    bui.with('examenes.examen')
                })
            })
            .with('examenes')
            .firstOrFail()
            return response.status(200).json(materia.toJSON() )
        }
        return view.render('profesor.materia.gestionar', { id });
    }
    async indexAlumno({ response, session}){
        const id_alumno = session.get('id_alumno');
        //console.log(await Database.fn.now())
        const materias = await Materia.query()
            .with('profesor')
            .with('alumnos', builder => {
                builder.where('alumno_id', id_alumno)
            })
            .where('fecha_inicio', '<', Database.fn.now())
            .where('fecha_termino', '>', Database.fn.now())
            .fetch()

        return response.status(200).json({materias})
    }
    async materiasInscrito({ request, response, auth, view, session }){
        if(request.format() === 'json'){
            const id_alumno = session.get('id_alumno');
            const materias = await AlumnoMateria.query()
                .where('alumno_id', id_alumno)
                .where('estatus', 'aceptado')
                .with('materia', builder => {
                    builder.with('profesor');
                    builder.with('alumnos', builder => {
                        builder.where('alumno_id', id_alumno)
                    })
                })
                .fetch() 
            return response.status(200).json({materias: materias}) 
        }    
        return view.render('alumno.materia.inscrito');
    }
    async indexProfesor({ request, response, auth, view, session }){
        if(request.format() === 'json'){
            const id_profesor = session.get('id_profesor');
            const materias = await  Materia.query()
                .where('profesor_id', id_profesor)
                .fetch()
            return response.status(200).json({ materias });
        }

        return view.render('profesor.materia.index');
        
    }

    async store({ request, response, auth, session}){
        const materiaData = request.only(["clave", "nombre", "area","fecha_inicio", "fecha_termino"]);
        const cover = request.file('file', {
            types: ['image'],
            size: '2mb'
        });
        let path_img = '/file/default.jpg'
        if(cover){
            await cover.move(Helpers.tmpPath('uploads'), {
                name: `${materiaData.clave}_${materiaData.nombre}.jpg`
            });
    
            if(!cover.moved()){
                return cover.error();
            }
            path_img = `/file/${materiaData.clave}_${materiaData.nombre}.jpg`
        }
        const id_profesor = session.get('id_profesor');
        //console.log("=>", materiaData)
        const materia = await Materia.create({ ...materiaData, profesor_id: id_profesor, path_img});
        return response.status(200).json({ message: 'Creada', materia });
    }

    async inscribirAlumno({request, response, session, auth }){
        const data = request.only(['materia_id']);
        const id_alumno = session.get('id_alumno');

        const alumnoMateria = await AlumnoMateria.create({ ...data, alumno_id: id_alumno });
        await alumnoMateria.load('alumno');
        await alumnoMateria.load('materia', builder => {
            builder.with('profesor', bui => {
                bui.with('usuario')
            })
        })
        Event.fire('notificarProfesor::usuario', alumnoMateria.toJSON());
        return response.status(200).json({ alumnoMateria, message: 'Solicitud enviada'})
    }
}

module.exports = MateriaController
