'use strict'

class AlumnoController {

    async index({ request, response, auth, view }){
        return view.render('alumno.index');
    }
}

module.exports = AlumnoController
