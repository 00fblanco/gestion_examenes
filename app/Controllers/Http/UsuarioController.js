'use strict'
const Usuario = use('App/Models/Usuario');
const Alumno = use('App/Models/Alumno');
const Profesor = use('App/Models/Profesor');
const Env = use('Env')
const Database = use("Database");
const Hash = use('Hash');
const generator = use('generate-password');
const Event = use('Event')
class UsuarioController {

    async resetContrasenia({request, response}){

        const usuario = await Usuario.query()
            .where('correo', request.input('correo'))
            .firstOrFail()
        const contrasenia_nva = await generator.generate({
            length: 10,
            numbers: true
        });
        usuario.contrasenia = contrasenia_nva;

        await usuario.save();

        Event.fire('resetContrasenia::usuario', {...usuario.toJSON(), contrasenia_nva: contrasenia_nva});
        return response.status(200).json({ message: 'Actualizado correctamente'})
        
    }
    async index({ request, response, view, auth, session }){
        await auth.logout()
        return view.render('usuarios/index');
    }

    async storeAlumno({ request, response }){
        const alumnoData = request.only(["no_control","nombre", "ap_paterno", "ap_materno", "sexo", "carrera"]);
        const usuarioData = request.only(["correo", "contrasenia"]);

        const trx = await Database.beginTransaction();
        const usuario = await Usuario.create({ ...usuarioData, rol: 'alumno'}, trx);
        const alumno = await Alumno.create({ ...alumnoData, usuario_id: usuario.id }, trx);
        response.status(200).json({ usuario, alumno });
        trx.commit();
    }

    async storeProfesor({ request, response }){
        const profesorData = request.only(["nombre", "ap_paterno", "ap_materno", "sexo"]);
        const usuarioData = request.only(["correo", "contrasenia"]);
        const master_contrasenia = request.input("master_contrasenia");
        if(master_contrasenia != Env.get('ADMIN_PASS', '')){
            return response.status(203).json({ message: 'Contraseña de administrador incorrecta. No se puede proseguir.'})
        }
        const trx = await Database.beginTransaction();
        const usuario = await Usuario.create({ ...usuarioData, rol: 'profesor'}, trx);
        const profesor = await Profesor.create({ ...profesorData, usuario_id: usuario.id }, trx);
        response.status(200).json({ usuario, profesor });
        trx.commit();
    }
    
    async login({ request, response, auth, session}){
        const {correo, contrasenia} = request.only(['correo', 'contrasenia']);
        
        const usuario = await Usuario
            .query()
            .where('correo', correo)
            .first()
        if(usuario === null){
            return response.status(203).json({ message: 'Usuario no encontrado'});
        }

        const verifyPassword = await Hash.verify(contrasenia, usuario.contrasenia);
        
        if(!verifyPassword)
            return response.status(203).json({ message: 'Contraseña incorrecta'});
        
        
        await auth.attempt(correo, contrasenia);
        
        if(usuario.rol === 'alumno'){
            const alumno = await Alumno.query().where('usuario_id', usuario.id).first()
            session.put('id_alumno', alumno.id)
        }else if(usuario.rol === 'profesor'){
            const profesor = await Profesor.query().where('usuario_id', usuario.id).first()
            session.put('id_profesor', profesor.id)
        }
        return response.status(200).json({ message: 'Bienvenido', rol: usuario.rol});

    }

    async logout({ response, auth, session }){
        await auth.logout()
        await session.clear()
        return response.redirect('/usuarios')
    }
}

module.exports = UsuarioController
