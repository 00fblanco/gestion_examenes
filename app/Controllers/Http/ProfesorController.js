'use strict'

class ProfesorController {

    async index({ request, response, auth, view }){
        return view.render('profesor.index');
    }
}

module.exports = ProfesorController
