'use strict'

const Examen = use('App/Models/Examen')
const AlumnoMateria = use('App/Models/AlumnoMateria');
const AlumnoExamen = use('App/Models/AlumnoExamen')
const docx = use('docx');
const fs = use('fs');
const Helpers = use('Helpers')
const moment = use('moment')
moment.locale('es')
class ExamanController {

    async generarReporte({ request, response }){
        const id = request.params.id;

        let data = await Examen.query()
            .where('id', id)
            .with('materia', query => {
                query.with("profesor")
                query.with("alumnos.alumno")
            })
            .with('alumnos')
            .firstOrFail()

        data = data.toJSON()
        console.log(data);
        var doc = new docx.Document();
        const banner = doc.createImage("public/img/banner.png");

        doc.createParagraph("Instituto Tecnológico de Chilpancingo, Gro.").heading1().center(); 
        doc.createParagraph("Sistema de gestión de exámenes ITCH").heading2().center();

        doc.createParagraph(`Num. alumnos: ${data.materia.alumnos.length}`).right();
        doc.createParagraph(`Lista de grupo ${data.materia.clave}`).heading2().left();
        doc.createParagraph(`Departamento: ${data.materia.area}`).left()
        doc.createParagraph(`Materia: ${data.materia.nombre}`).left()
        doc.createParagraph(`Profesor (a): ${data.materia.profesor.nombre} ${data.materia.profesor.ap_paterno} ${data.materia.profesor.ap_materno}`).left()
        doc.createParagraph(`Periodo: ${moment(data.materia.fecha_inicio, 'YYY-MM-DD').format('LL')} - ${moment(data.materia.fecha_termino, 'YYY-MM-DD').format('LL')}`).left()
        doc.createParagraph(`Examen: ${data.nombre}`).left()
        doc.createParagraph(`Fecha de aplicación: ${moment(data.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss').format('LLLL')}`).left();
        doc.createParagraph(`Tiempo de aplicación ${moment(data.tiempo_aplicacion, 'HH:mm:ss').format('LT')} Hrs.`).left()
        
        const table = doc.createTable((data.materia.alumnos.length + 1 ), 5);
        table.getCell(0, 0).addContent(new docx.Paragraph("No"));
        table.getCell(0, 1).addContent(new docx.Paragraph("No.Control"));
        table.getCell(0, 2).addContent(new docx.Paragraph("Nombre"));
        table.getCell(0, 3).addContent(new docx.Paragraph("Carrera"));
        table.getCell(0, 4).addContent(new docx.Paragraph("Calificación"));
        data.materia.alumnos.map((row, index) => {
            index += 1; 
            let alumnoExamen = data.alumnos.find(alumno => alumno.alumno_id === row.alumno.id);
            table.getCell(index, 0).addContent(new docx.Paragraph(`${index}`));
            table.getCell(index, 1).addContent(new docx.Paragraph(`${row.alumno.no_control}`));
            table.getCell(index, 2).addContent(new docx.Paragraph(`${row.alumno.nombre_completo}`));
            table.getCell(index, 3).addContent(new docx.Paragraph(`${row.alumno.carrera}`));
            table.getCell(index, 4).addContent(new docx.Paragraph(`${alumnoExamen ? alumnoExamen.calificacion : 'No presento'}`));
        });

        const exporter = new docx.LocalPacker(doc);
        if(request.params.tipo === 'pdf'){
            console.log('entra pdf')
            await exporter.packPdf(Helpers.tmpPath('files/reporte'));
            console.log('sale pdf')
            return response.download(Helpers.tmpPath('files/reporte.pdf'))
        }else{
            await exporter.pack(Helpers.tmpPath('files/reporte'));
            return response.download(Helpers.tmpPath('files/reporte.docx'))
        }
    }
    async calificacion({request, response, session }){
        const alumno_id = session.get('id_alumno');
        const data = request.only(['examen_id', 'calificacion']);

        const alumnoExamen = await AlumnoExamen
            .query()
            .where('alumno_id', alumno_id)
            .where('examen_id', data.examen_id)
            .firstOrFail()
        alumnoExamen.merge({
            calificacion: data.calificacion
        })
        await alumnoExamen.save();
        return response.status(200).json({ messsage: 'Actualziado satisfactoriamente'})
    }
    async realizar({request, view, session, response }){
        const examen_id = request.params.id
        const alumno_id = session.get('id_alumno');
        if(request.format() === 'json'){
            // verificamos si el alumno ya hizo el examen :O
            const exists = await AlumnoExamen
                .query()
                .select('id')
                .where('alumno_id', alumno_id)
                .where('examen_id', examen_id)
                .first()
            if(exists !== null){
                return response.status(203).json({message: 'Ya ha realizado este examen.'});
            }
            const alumnoExamen = await AlumnoExamen.create({
                alumno_id,
                examen_id
            });
            const materia = await Examen.query()
                .select('materia_id')
                .where('id', examen_id)
                .firstOrFail();

            const data = await AlumnoMateria.query()
                .where('estatus', 'aceptado')
                .where('alumno_id', alumno_id)
                .where('materia_id', materia.materia_id)
                .with('materia', matBuilder => {
                    matBuilder.with('examenes', examBuilder => {
                        examBuilder.where('id', examen_id)
                        examBuilder.with('preguntas', preguntasBuilder => {
                            preguntasBuilder.with('respuestas', _builder => {
                                _builder.with('respuesta')
                            })
                        })
                    });
                    matBuilder.with('profesor')
                }).firstOrFail()
            return response.status(200).json(data)
        }

        return view.render('alumno.examen.index', {id: examen_id})

    }
}

module.exports = ExamanController
