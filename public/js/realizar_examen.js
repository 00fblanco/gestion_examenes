new Vue({
    el: '#app',
    data(){
        return {
            id: 0,
            respuesta: '',
            respuestas_relacion: [],
            respuestas: [],
            puntuacion: 0,
            puntuacion_max: 0,
            calificacion: -1,
            materia: {},
            profesor: {},
            examen: {
                preguntas: []
            },
            respuestasDrop: [],
            formExamen: {
            },
            preguntaActiva: 0,
        }
    },
    created(){
        this.loadExamen(window.id)
    },
    methods: {
        handleDrop(index,pregunta_id, data){
            this.respuesta = 'nomas';
            let index_pregunta = this.examen.preguntas.findIndex(pregunta => pregunta.id === pregunta_id);
            console.log(this.respuestasDrop[index_pregunta].respuestas[index].respuesta)
            let newRespuestas = this.respuestasDrop[index_pregunta].respuestas.map((resp, i) => {
                if(i === index){
                    return {
                        respuesta: data.respuesta,
                        id: data.id
                    }
                }
                return {
                    ...resp
                }

            })
            console.log(newRespuestas)
            console.log(this.respuestasDrop[index_pregunta])
            this.respuestasDrop[index_pregunta].respuestas = newRespuestas;
            //alert(`index ${index} ${pregunta_id}: ${JSON.stringify(data)}`);
        },
        evaluaExamenRelacion(){
            if(this.examen.preguntas.length === this.respuestas.length){
                this.examen.preguntas.map((pregunta, i) => {
                    let respuesta = pregunta.respuestas.find(resp => resp.respuesta_id === this.respuestas[i]) || null;
                    if(respuesta === null || respuesta.esCorrecta === 'no'){
                        this.$message({
                            showClose: true,
                            message: `Respuesta incorrecta: \n${pregunta.retroalimentacion}`,
                            type: 'warning'
                        });
                    }else if(respuesta !== null && respuesta.esCorrecta === 'si'){
                        this.$message({
                            message: `Respuesta correcta`,
                            type: 'success'
                        });
                        this.puntuacion = this.puntuacion + pregunta.puntuacion
                    }
                    
                });
                // califica examen 
                let calificacion = (this.puntuacion*100)/this.puntuacion_max;
                if(calificacion > 50){
                    this.$message({
                        showClose: true,
                        message: `Has pasado el examen tu calificación es: ${calificacion}`,
                        type: 'success'
                    });
                }else{
                    this.$message({
                        showClose: true,
                        message: `No has pasado el examen tu calificación es: ${calificacion}`,
                        type: 'warning'
                    });
                }
                this.calificacion = calificacion
                axios.put('/alumno/examen', {
                    examen_id: this.examen.id,
                    calificacion: calificacion
                }).then(resp => {
                    if(resp.status === 200){
                        this.$message({
                            message: `Calificaciòn guardada en la bd`,
                            type: 'error'
                        });
                    }
                })
                .catch(err => {
                    console.log("err - " + err)
                    this.$message({
                        message: `Error al cargar las materias.`,
                        type: 'error'
                    });
                })
            }else{
                this.$message({
                    message: `Debe responder todas las preguntas`,
                    type: 'warning'
                });
                return;
            }
        },
        next(){
            //console.log(this.examen.preguntas.length)
            //sconsole.log(JSON.stringify(this.examen.preguntas))
            if(this.preguntaActiva < this.examen.preguntas.length && this.respuesta !== ''){
                let pregunta = this.examen.preguntas[this.preguntaActiva]
                if(pregunta.tipo === 'opc_mult' || pregunta.tipo === 'verdadero_falso' || pregunta.tipo === 'emparejamiento'){
                    let respuesta = pregunta.respuestas.find(resp => resp.id === this.respuesta);
                    if(respuesta.esCorrecta === 'no'){
                        this.$message({
                            showClose: true,
                            message: `Respuesta incorrecta: \n${pregunta.retroalimentacion}`,
                            type: 'warning'
                        });
                    }else if(respuesta.esCorrecta === 'si'){
                        this.$message({
                            message: `Respuesta correcta`,
                            type: 'success'
                        });
                        this.puntuacion = this.puntuacion + pregunta.puntuacion
    
                    }
                }else if(pregunta.tipo === 'resp_corta'){
                    let respuesta = pregunta.respuestas[0];
                    if(respuesta.respuesta.respuesta === this.respuesta){
                        this.$message({
                            message: `Respuesta correcta`,
                            type: 'success'
                        });
                        this.puntuacion = this.puntuacion + pregunta.puntuacion
                    }else{
                        this.$message({
                            showClose: true,
                            message: `Respuesta incorrecta: \n${pregunta.retroalimentacion}`,
                            type: 'warning'
                        });
                    }
                }else if(pregunta.tipo === 'arrastrar_soltar'){
                    
                    let respuestas = this.respuestasDrop.find(resp => resp.pregunta_id === pregunta.id).respuestas;
                    let esCorrecta = true;
                    for(let i = 0; i < pregunta.respuestas.length; i++){
                        if(pregunta.respuestas[i].respuesta_id !== respuestas[i].id){
                            esCorrecta = false;
                            break;
                        }
                    }
                    if(esCorrecta){
                        this.$message({
                            message: `Respuesta correcta`,
                            type: 'success'
                        });
                        this.puntuacion = this.puntuacion + pregunta.puntuacion;
                    }else{
                        this.$message({
                            showClose: true,
                            message: `Respuesta incorrecta: \n${pregunta.retroalimentacion}`,
                            type: 'warning'
                        });
                    }
                }
                this.respuesta = ''
                this.preguntaActiva++
            }else if(this.respuesta === '' && this.preguntaActiva !== this.examen.preguntas.length){
                this.$message({
                    message: `Debe responder la pregunta`,
                    type: 'warning'
                });
                return;
            }
            if(this.preguntaActiva === this.examen.preguntas.length){
                this.$message({
                    message: `Termino el examen :)`,
                    type: 'success'
                });   
                // califica examen 
                let calificacion = (this.puntuacion*100)/this.puntuacion_max;
                if(calificacion > 50){
                    this.$message({
                        showClose: true,
                        message: `Has pasado el examen tu calificación es: ${calificacion}`,
                        type: 'success'
                    });
                }else{
                    this.$message({
                        showClose: true,
                        message: `No has pasado el examen tu calificación es: ${calificacion}`,
                        type: 'warning'
                    });
                }
                this.calificacion = calificacion
                axios.put('/alumno/examen', {
                    examen_id: this.examen.id,
                    calificacion: calificacion
                }).then(resp => {
                    if(resp.status === 200){
                        this.$message({
                            message: `Calificaciòn guardada en la bd`,
                            type: 'success'
                        });
                    }
                })
                .catch(err => {
                    console.log("err - " + err)
                    this.$message({
                        message: `Error al cargar las materias.`,
                        type: 'error'
                    });
                })
                return;
            }

        },
        loadExamen(id){
            axios.get(`/alumno/examen/${id}.json`)
                .then(response => {
                    if(response.status === 200){
                        this.materia = response.data.materia
                        this.profesor = response.data.materia.profesor
                        this.examen = response.data.materia.examenes[0]
                        let puntuacion_max = 0;
                        response.data.materia.examenes[0].preguntas.map(pregunta => {
                            puntuacion_max = puntuacion_max + pregunta.puntuacion;
                        })
                        this.puntuacion_max = puntuacion_max;
                        //console.log('max', puntuacion_max)
                        if(response.data.materia.examenes[0].preguntas[0].tipo === 'relacion'){
                            response.data.materia.examenes[0].preguntas.map(pregunta => {
                                pregunta.respuestas.map(resp => {
                                    this.respuestas_relacion.push(resp)    
                                })
                            })
                        }else if(response.data.materia.examenes[0].preguntas[0].tipo === 'arrastrar_soltar'){
                            let respuestasDrop = [];
                            let preguntas =  response.data.materia.examenes[0].preguntas.map(pregunta => {
                                let partesPregunta = pregunta.descripcion.split(/\[[a-zA-Z]+\]/); 

                                let resps = pregunta.respuestas.map(resp => {
                                    return {
                                        respuesta: 'vv',
                                        id: 0
                                    }
                                });
                                respuestasDrop.push({
                                    pregunta_id: pregunta.id,
                                    respuestas: resps
                                })
                                return {
                                    ...pregunta,
                                    partesPregunta,
                                }
                            });
                            
                            this.respuestasDrop = respuestasDrop;
                            console.log('=>', this.respuestasDrop)
                            this.examen = {...response.data.materia.examenes[0], preguntas};
                        }

                    }else {
                        this.$message({
                            message: `Error: ${response.data.message}`,
                            type: 'error'
                        });
                    }
                    
                }).catch(err => {
                    console.log("err - " + err)
                    this.$message({
                        message: `Error al cargar las materias.`,
                        type: 'error'
                    });
                })
        }
    }
})