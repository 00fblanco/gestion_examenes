const crearMateria = new Vue({
    el: '#app',
    data(){
        return {
            materia_id: 0,
            pregunta_id: 0,
            respuesta_id: 0,
            dialogPregunta: false,
            dialogRespuesta: false,
            preguntas: [],
            respuestas_opcion: [],
            respuestas_relacion: [],
            respuestas_emparejamiento: [],
            preguntas_respuestas: [],
            formExamen: {
                nombre: '',
                descripcion: '',
                tipo: '',
                fecha_aplicacion: '',
                tiempo_aplicacion: '',
                materia_id: '',
                tipo_pregunta: ''
            },
            formPregunta: {
                pregunta_id: 0,
                descripcion: '',
                puntuacion: '',
                retroalimentacion: '',
                verdadero_falso: '',
                resp_corta: '',
                emparejamiento: '',
            },
            formRespuesta: {
                pregunta_id: '',
                pregunta_tipo: '',
                pregunta_descripcion: '',
                respuesta: '',
                esCorrecta: 'no'
            },
            rulesExamen: {
                nombre:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                descripcion:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                tipo:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                fecha_aplicacion:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                tiempo_aplicacion:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                tipo_pregunta: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
            },
            rulesRespuesta: {
                respuesta:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                esCorrecta:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
            },
            rulesPregunta: {
                descripcion: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                puntuacion: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                verdadero_falso:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                resp_corta:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                emparejamiento:  [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
            }
        }
    },
    created(){
        //console.log(window.id)
        this.materia_id = window.id;
    },
    methods: {
        borrarPreguntas(){ // bug aqui manda error cuando cambias de tipo de pregunta
            
            if(this.pregunta_id > 0){
                alert('borra')
                this.preguntas = []
                this.pregunta_id =  0,
                this.respuesta_id =  0,
                this.respuestas_opcion = []
                this.respuestas_relacion =  []
                this.respuestas_emparejamiento =  []
                this.preguntas_respuestas = []
            }
        },
        showPregunta(){
            if(this.formExamen.tipo_pregunta !== ''){
                this.dialogPregunta = true;
            }else{
                this.$message({
                    message: 'Ingrese el tipo de pregunta',
                    type: 'warning'
                });
            }
        },
        loadFormRespuesta(row){
            this.formRespuesta.pregunta_tipo = row.tipo
            this.formRespuesta.pregunta_descripcion = row.descripcion;
            this.formRespuesta.pregunta_id = row.pregunta_id;
        },
        addRespuesta(id_pregunta){
            this.$refs['formRespuesta'].validate((valid) => {
                if(valid){
                    // this.preguntas[id_pregunta].respuestas.push({
                    //     respuesta: this.formRespuesta.respuesta,
                    //     esCorrecta: this.formRespuesta.esCorrecta
                    // });

                    switch(this.formRespuesta.pregunta_tipo){
                        case('opc_mult'):
                            this.respuestas_opcion.push({
                                pregunta_id: this.formRespuesta.pregunta_id,
                                respuesta_id: this.respuesta_id,
                                respuesta: this.formRespuesta.respuesta,
                                esCorrecta: this.formRespuesta.esCorrecta
                            })
                            this.respuesta_id = this.respuesta_id + 1;
                            break;
                        case('relacion'):
                            this.respuestas_relacion.push({
                                pregunta_id: this.formRespuesta.pregunta_id,
                                respuesta_id: this.respuesta_id,
                                respuesta: this.formRespuesta.respuesta,
                                esCorrecta: this.formRespuesta.esCorrecta
                            })
                            this.respuesta_id = this.respuesta_id + 1;
                            break;
                        default:
                            this.$message({
                                message: `Tipo de pregunta no reconocia: ${this.formRespuesta.pregunta_tipo}`,
                                type: 'warning'
                            });
                            break;
                    }
                    // console.log(JSON.stringify(this.preguntas[id_pregunta]));
                    this.resetForm('formRespuesta')
                    this.dialogRespuesta = false;
                }else{
                    this.$message({
                        message: 'Revise los campos',
                        type: 'warning'
                    });
                    return false;
                }
            });
        },
        addPregunta(){
            //console.log(this.formPregunta)
            this.$refs['formPregunta'].validate((valid) => {
                if(valid){
                    if(this.formExamen.tipo_pregunta === 'resp_corta'){
                        //^[a-zA-z ]+\\[\\][a-zA-z ]+$
                        if(this.formPregunta.descripcion.search("^[a-zA-zñÑáéíóúÁÉÍÓÚ\\?\\¿\\,\\. ]+\\[\\][a-zA-zñÑáéíóúÁÉÍÓÚ\\?\\¿\\,\\. ]+$") < 0){
                            this.$message({
                                message: 'Debe de seguir la reglas de la resp_corta',
                                type: 'warning'
                            });
                            return false;
                        }
                    }else if(this.formExamen.tipo_pregunta === 'arrastrar_soltar'){
                        if(this.formPregunta.descripcion.search("^([a-zA-zñÑáéíóúÁÉÍÓÚ\\?\\¿\\,\\. ]+\\[[a-zA-zñÑáéíóúÁÉÍÓÚ ]+\\][[a-zA-zñÑáéíóúÁÉÍÓÚ\\?\\¿\\,\\. ]*]?)+$") < 0){
                            this.$message({
                                message: 'Debe de seguir la reglas descritas',
                                type: 'warning'
                            });
                            return false;
                        }
                    }
                    this.preguntas.push({
                        pregunta_id: this.pregunta_id,
                        descripcion: this.formPregunta.descripcion,
                        tipo: this.formExamen.tipo_pregunta,
                        puntuacion: this.formPregunta.puntuacion,
                        retroalimentacion: this.formPregunta.retroalimentacion,
                    });
                    if(this.formExamen.tipo_pregunta === 'verdadero_falso'){
                        this.respuestas_opcion.push({
                            pregunta_id: this.pregunta_id,
                            respuesta_id: this.respuesta_id,
                            respuesta: "Verdadero",
                            esCorrecta: this.formPregunta.verdadero_falso === 'si' ? 'si' : 'no'
                        })
                        this.respuesta_id = this.respuesta_id + 1;
                        this.respuestas_opcion.push({
                            pregunta_id: this.pregunta_id,
                            respuesta_id: this.respuesta_id,
                            respuesta: "Falso",
                            esCorrecta: this.formPregunta.verdadero_falso === 'no' ? 'si' : 'no'
                        })
                        this.respuesta_id = this.respuesta_id + 1;
                    }else if(this.formExamen.tipo_pregunta === 'resp_corta'){
                        this.respuestas_opcion.push({
                            pregunta_id: this.pregunta_id,
                            respuesta_id: this.respuesta_id,
                            respuesta: this.formPregunta.resp_corta,
                            esCorrecta: "si"
                        })
                        this.respuesta_id = this.respuesta_id + 1;
                    }else if(this.formExamen.tipo_pregunta === 'arrastrar_soltar'){
                        //console.log(this.formPregunta.descripcion)
                        let regex = /\[(.*?)\]/g;
                        while(match = regex.exec(this.formPregunta.descripcion)){
                            //match[1] <= clean match
                            this.respuestas_opcion.push({
                                pregunta_id: this.pregunta_id,
                                respuesta_id: this.respuesta_id,
                                respuesta: match[1],
                                esCorrecta: 'si'
                            })
                            this.respuesta_id = this.respuesta_id + 1;
                        }

                    }
                    else if(this.formExamen.tipo_pregunta === 'emparejamiento'){
                        this.respuestas_emparejamiento.push({
                            pregunta_id: this.pregunta_id,
                            respuesta_id: this.respuesta_id,
                            respuesta: this.formPregunta.emparejamiento,
                            esCorrecta: "si"
                        })
                        this.respuesta_id = this.respuesta_id + 1;
                    }
                    
                    this.pregunta_id = this.pregunta_id + 1;
                    this.resetForm('formPregunta')
                    this.dialogPregunta = false;
                }else{
                    this.$message({
                        message: 'Revise los campos',
                        type: 'warning'
                    });
                    return false;
                }
            });
        },
        resetForm(formName) {
            this.$refs[formName].resetFields();
        },
        disableDate(datetime){
            return false;
        },
        saveExamen(){
            // console.log(JSON.stringify(this.preguntas))
            // console.log(JSON.stringify(this.respuestas_opcion))
            // return;
            this.$refs['formExamen'].validate(valid => {
                if(valid){
                    if(this.preguntas.length === 0){
                        this.$message({
                            message: 'El examen debe tener preguntas',
                            type: 'warning'
                        });
                        return false;
                    }
                    let err = 0;
                    this.preguntas.map(pregunta => {
                        if(pregunta.tipo === 'opc_mult' || pregunta.tipo === 'verdadero_falso' || pregunta.tipo === 'resp_corta'){
                            if(this.respuestas_opcion.filter(resp => (resp.pregunta_id === pregunta.pregunta_id) && (resp.esCorrecta === 'si') ).length === 0){
                                err++;
                            }
                        }
                        if(pregunta.tipo === 'relacion'){
                            if(this.respuestas_relacion.filter(resp => (resp.pregunta_id === pregunta.pregunta_id) && (resp.esCorrecta === 'si') ).length === 0){
                                err++;
                            }
                        }
                    })

                    if(err > 0){
                        this.$message({
                            message: 'No pueden existir preguntas sin respuestas, favor de revisar',
                            type: 'warning'
                        });
                        return false;
                    }
                    axios.post('/materia/examen', {
                        materia_id: this.materia_id,
                        nombre: this.formExamen.nombre,
                        descripcion: this.formExamen.descripcion,
                        fecha_aplicacion: this.formExamen.fecha_aplicacion,
                        tiempo_aplicacion: this.formExamen.tiempo_aplicacion,
                        tipo: this.formExamen.tipo,
                        preguntas: this.preguntas,
                        respuestas_opcion: this.respuestas_opcion,
                        respuestas_emparejamiento: this.respuestas_emparejamiento,
                        respuestas_relacion: this.respuestas_relacion,
                    }).then(response =>{
                        if(response.status === 200){
                            this.$message({
                                message: 'Examen creado.',
                                type: 'success'
                            })
                            window.location.href=`/profesor/materia/${this.materia_id}`;
                        }else{
                            this.$message({
                                message: 'Error al crear examen: ' + response.data.message,
                                type: 'warning'
                            })
                        }
                    })


                }else{
                    this.$message({
                        message: 'Revise los campos',
                        type: 'warning'
                    });
                    return false;
                }
            })
        }
    }
    
})