const gestionarMaterias = new Vue({
    el: '#app',
    data(){
        return {
            visibleEditarMateria: false,
            examenes: [],
            materia: {},
            profesor: {},
            alumnos: [],
            activeTab: 'alumnos',
            formMateria: {},
            rulesMateria: {
                nombre: [
                    { required: true, message: 'Campo requeridoa', trigger: ['blur', 'change'] }
                ],
                area: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                fecha_inicio: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                fecha_termino: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
            }
        }
    },
    created(){
        //console.log(window.id)
        this.getMateria(window.id)
    },
    methods: {
        filterEstatus(value, row) {
            return row.estatus === value;
        },
        eliminarExamen(id){
             axios.delete(`/materia/examen/${id}`)
             .then(response => {
                if(response.status === 200){
                    window.location.reload(); 
                    
                }else{
                    this.$message({
                        message: `Error al aceptar alumno`,
                        type: 'error'
                    })
                }
                   
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error con la solicitud.`,
                    type: 'error'
                });
            });
        },
        actualizarMateria(){
            axios.put('/materia', {
                materia_id: this.formMateria.materia_id,
                nombre: this.formMateria.nombre, 
                fecha_inicio: moment(this.formMateria.fecha_inicio, 'YYYY-MM-DD').format('YYYY-MM-DD'), 
                fecha_termino: moment(this.formMateria.fecha_termino, 'YYYY-MM-DD').format('YYYY-MM-DD'), 
                area: this.formMateria.area 
            }).then(response => {
                if(response.status === 200){
                    window.location.reload(); 
                    
                }else{
                    this.$message({
                        message: `Error al aceptar alumno`,
                        type: 'error'
                    })
                }
                   
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error con la solicitud.`,
                    type: 'error'
                });
            });
        },
        resetForm(formName) {
            this.$refs[formName].resetFields();
        },
        aceptarSolicitud(alumno_id){
            axios.put(`/materia/solicitud`, {
                estatus: 'aceptado',
                alumno_id,
                materia_id: this.materia.id
            }).then(response => {
                if(response.status === 200){
                    this.getMateria(this.materia.id);
                }else{
                    this.$message({
                        message: `Error al aceptar alumno`,
                        type: 'error'
                    });
                }
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error con la solicitud.`,
                    type: 'error'
                });
            })
        },
        rechazarSolicitud(alumno_id){
            axios.put(`/materia/solicitud`, {
                estatus: 'rechazado',
                alumno_id,
                materia_id: this.materia.id
            }).then(response => {
                if(response.status === 200){
                    this.getMateria(this.materia.id);
                }else{
                    this.$message({
                        message: `Error al rechazar alumno`,
                        type: 'error'
                    });
                }
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error con la solicitud.`,
                    type: 'error'
                });
            })
        },
        getMateria(id){
            axios.get(`/profesor/materia/${id}.json`)
                .then(response => {
                    if(response.status === 200){
                        this.materia = response.data
                        this.profesor = response.data.profesor
                        this.alumnos = response.data.alumnos
                        this.examenes = response.data.examenes.map(examen => {
                            //console.log(examen.fecha_aplicacion)
                            return {
                                ...examen,
                                fecha_aplicacion: moment(examen.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss').format('LLLL'),
                                tiempo_aplicacion: moment(examen.tiempo_aplicacion, 'HH:mm:ss').format('LT')
                            }
                        })
                        this.formMateria = {
                            materia_id: response.data.id,
                            nombre: response.data.nombre,
                            fecha_inicio: response.data.fecha_inicio,
                            fecha_termino: response.data.fecha_termino,
                            area: response.data.area
                        }
                    }else {
                        this.$message({
                            message: `Error: ${response.data.message}`,
                            type: 'error'
                        });
                    }
                    
                }).catch(err => {
                    console.log("err - " + err)
                    this.$message({
                        message: `Error al cargar las materias.`,
                        type: 'error'
                    });
                })
        }
    }
})