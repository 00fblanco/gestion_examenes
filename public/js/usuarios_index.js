
const controlador_welcome = new Vue({
    el: "#app",
    data(){
        var checkPassSignup = (rule, value, callback) => {
            if (value === '') {
                callback(new Error('Requerido para confirmar contraseña'));
            } else if (value !== this.formSignup.contrasenia) {
                callback(new Error('No coincide la confirmación de contraseña'));
            } else {
                callback();
            }
        };
        var checkPassProfesor = (rule, value, callback) => {
            if (value === '') {
                callback(new Error('Requerido para confirmar contraseña'));
            } else if (value !== this.formProfesor.contrasenia) {
                callback(new Error('No coincide la confirmación de contraseña'));
            } else {
                callback();
            }
        };
        return {
            showPass: false,
            page: 'login' ,
            activeName: 'registrarAlumno',
            formLogin: {
                correo: '',
                contrasenia: ''
            },
            rulesLogin: {
                correo: [
                    { required: true, message: 'Ingrese su correo', trigger: 'blur' },
                    { type: 'email', message: 'Ingrese un correo correcto', trigger: ['blur', 'change'] }
                ],
                contrasenia: [
                    { required: true, message: 'Ingrese su contraseña', trigger: 'blur' },
                ]
            },
            formSignup: {
                no_control: '',
                nombre: '',
                ap_paterno: '',
                ap_materno: '',
                sexo: '',
                correo: '',
                contrasenia: '',
                check_contrasenia: '',
                carrera: ''
            },
            rulesSignup: {
                no_control: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                carrera: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                nombre: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                ap_paterno: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                ap_materno: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                sexo: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                correo: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                    { type: 'email', message: 'Ingrese un correo correcto', trigger: ['blur', 'change'] }
                ],
                contrasenia: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                check_contrasenia: [
                    { validator: checkPassSignup, trigger: ['blur', 'change'] }
                ],
                
            },
            formProfesor: {
                nombre: '',
                ap_paterno: '',
                ap_materno: '',
                sexo: '',
                correo: '',
                contrasenia: '',
                check_contrasenia: '',
                master_contrasenia: ''
            },
            rulesProfesor: {
                nombre: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                ap_paterno: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                ap_materno: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                sexo: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                correo: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                    { type: 'email', message: 'Ingrese un correo correcto', trigger: ['blur', 'change'] }
                ],
                contrasenia: [
                    { required: true, message: 'Requerido', trigger: ['blur'] },
                ],
                check_contrasenia: [
                    { validator: checkPassProfesor, trigger: ['blur', 'change'] }
                ],
                master_contrasenia: [
                    { required: true, message: 'Se necesita la contraseña de admin.', trigger: ['blur'] },
                ],
                
            }
            
        }
    },
    methods: {
        resetContrasenia(){
            if(this.formLogin.correo === ''){
                this.$message({
                    message: `Debe ingresar su correo`,
                    type: 'warning'
                });   
            }
            axios.put('/usuarios/reset_contrasenia', {
                correo: this.formLogin.correo
            }).then(response => {
                if(response.status === 200){
                    this.$message({
                        message: `Se envio un correo con la restauración de su contraseña a ${this.formLogin.correo}`,
                        type: 'info'
                    });
                }else{
                    this.$message({
                        message: `Error (No existe usuario): ${response.data.message}`,
                        type: 'warning'
                    });
                }
            }).catch(err => {
                this.$message({
                    message: `Error: ${err}`,
                    type: 'warning'
                });
            })
        },
        handleClick(tab, event){
            console.log(tab, event);
        },
        resetForm(formName) {
            this.$refs[formName].resetFields();
        },
        submitForm(formName){
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    if(formName === 'formSignup'){
                        // mandar peticion a el server 
                        axios.post('/usuarios/alumno',{
                            carrera: this.formSignup.carrera,
                            no_control: this.formSignup.no_control,
                            nombre: this.formSignup.nombre,
                            ap_paterno: this.formSignup.ap_paterno,
                            ap_materno: this.formSignup.ap_materno,
                            sexo: this.formSignup.sexo,
                            correo: this.formSignup.correo,
                            contrasenia: this.formSignup.contrasenia
                        }).then(response => {
                            console.log("server - ", response);
                            if(response.status === 200){
                                this.$alert('Alumno creado correctamente, inicie sesión', 'Mensaje', {
                                    confirmButtonText: 'Aceptar',
                                    callback: action => {
                                      
                                    }
                                  });
                                this.page="login"
                            }else{
                                this.$alert('No se pudo crear el profesor, intente mas tarde', 'Mensaje', {
                                    confirmButtonText: 'Aceptar',
                                    callback: action => {
                                      
                                    }
                                  });
                            }
                        }).catch(err => {
                            console.log("err - " + err)
                            this.$alert('No se pudieron guardar los datos favor de revisar', 'Mensaje', {
                                confirmButtonText: 'Aceptar',
                                callback: action => {
                                  
                                }
                              });
                        })
                    }else if(formName === 'formLogin'){
                        axios.post('/usuarios/login', {
                            correo: this.formLogin.correo,
                            contrasenia: this.formLogin.contrasenia
                        }).then(response => {
                            console.log("res - ", response);
                            if(response.status === 200){
                                this.$message({
                                    message: `Bienvenido ${response.data.rol}`
                                });
                                if(response.data.rol === 'alumno'){
                                    window.location.href="/alumno";
                                }else if(response.data.rol === 'profesor'){
                                    window.location.href="/profesor"
                                }
                                
                            }else{
                                this.$message({
                                    message: `Error: ${response.data.message}`,
                                    type: 'error'
                                });
                            }
                        }).catch(err => {
                            console.log("err - " + err)
                            this.$message({
                                message: `Error de conexión ${err.data.message}`,
                                type: 'error'
                            });
                        })
                    }else if(formName === "formProfesor"){
                        axios.post('/usuarios/profesor', {
                            nombre: this.formProfesor.nombre,
                            ap_paterno: this.formProfesor.ap_paterno,
                            ap_materno: this.formProfesor.ap_materno,
                            sexo: this.formProfesor.sexo,
                            correo: this.formProfesor.correo,
                            contrasenia: this.formProfesor.contrasenia,
                            master_contrasenia: this.formProfesor.master_contrasenia
                        }).then(response => {
                            console.log("server - ", response);
                            if(response.status === 200){
                                this.$alert('Profesor creado correctamente, inicie sesión', 'Mensaje', {
                                    confirmButtonText: 'Aceptar',
                                    callback: action => {
                                      
                                    }
                                  });
                                this.page="login"
                            }else{
                                this.$alert('No se pudo crear el profesor, intente mas tarde', 'Mensaje', {
                                    confirmButtonText: 'Aceptar',
                                    callback: action => {
                                      
                                    }
                                  });
                            }
                        }).catch(err => {
                            console.log("err - " + err)
                            this.$alert('No se pudieron guardar los datos favor de revisar', 'Mensaje', {
                                confirmButtonText: 'Aceptar',
                                callback: action => {
                                  
                                }
                              });
                        })
                    }
                } else {
                    this.$alert('Revise los campos', 'Mensaje', {
                        confirmButtonText: 'Aceptar',
                        callback: action => {
                          
                        }
                      });
                    
                  return false;
                }
              });
        }
    }
})