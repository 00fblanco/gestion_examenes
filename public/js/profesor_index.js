const controlador_alumno = new Vue({
    el: '#app',
    data(){
        return {
            deleteConfirmation: false,
            visibleAddMateria: false,
            materias: [],
            formMateria:{
                file: '',
                clave: '',
                nombre: '',
                path_img: '',
                fecha_inicio: '',
                fecha_termino: '',
                area: '',
            },
            rulesMateria: {
                clave: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                nombre: [
                    { required: true, message: 'Campo requeridoa', trigger: ['blur', 'change'] }
                ],
                area: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                fecha_inicio: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
                fecha_termino: [
                    { required: true, message: 'Campo requerido', trigger: ['blur', 'change'] }
                ],
            }

        }
    },
    created() {
        this.getMaterias();
    },
    methods: {
        eliminarMateria(materia_id){
            axios.delete('/materia/'+materia_id)
            .then(response => {
                if(response.status === 200){
                    this.$message({
                        message: `Materia eliminada`,
                        type: 'success'
                    });
                    // redirect
                    window.location.href = '/profesor/materias'
                }
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error al cargar las materias.`,
                    type: 'error'
                });
            })
        },
        handleFileUpload(){
            console.log(this.$refs['file'].files[0]);
            this.file = this.$refs.file.files[0];
        },
        getMaterias(){
            axios.get('/profesor/materias.json')
            .then(response => {
                this.materias = response.data.materias.map(materia => {
                    return {
                        ...materia,
                        fecha_inicio: moment(materia.fecha_inicio).utc().format('YYYY-MM-DD'),
                        fecha_termino: moment(materia.fecha_termino).utc().format('YYYY-MM-DD')
                    }
                })
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error al cargar las materias.`,
                    type: 'error'
                });
            })
        },
        resetForm(formName) {
            this.$refs[formName].resetFields();
        },
        submitForm(formName){
            
            this.$refs[formName].validate((valid) => {
                if(valid){
                    let formData = new FormData();
                    formData.append('file', this.file);
                    formData.append('clave', this.formMateria.clave);
                    formData.append('nombre', this.formMateria.nombre);
                    formData.append('area', this.formMateria.area);
                    formData.append('fecha_inicio', this.formMateria.fecha_inicio);
                    formData.append('fecha_termino', this.formMateria.fecha_termino);
                    formData.append('csrf-token', "{{ csrfToken }}");
                    axios.post('/profesor/materia',
                        formData
                    , {headers: {'Content-Type': 'multipart/form-data' }}
                    ).then(response => {
                        console.log("server - ", response);
                        if(response.status === 200){
                            this.$message({
                                message: 'Materia agregada'
                            });
                            this.resetForm('formMateria');
                            this.materias.push(response.data.materia)
                        }else{
                            this.$message({
                                message: `No se pudieron guardar los datos:  ${response.data.message}`,
                                type: 'error'
                            });
                        }
                    }).catch(err => {
                        console.log("err - " + err)
                        this.$message({
                            message: `No se pudieron guardar los datos, favor de revisar.`,
                            type: 'error'
                        });
                    })
                }else{
                    this.$message({
                        message: 'Revise los campos',
                        type: 'warning'
                    });
                    return false;
                }
            });
        }
    }
})