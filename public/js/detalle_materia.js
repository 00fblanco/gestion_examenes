new Vue({
    el: '#app',
    data(){
        return {
            materia: {},
            profesor: {},
            examenes: []
        }
    },
    created(){
        this.loadMateria(window.id);
    },
    methods: {
        loadMateria(id){
            axios.get(`/alumno/materia/${id}.json`)
                .then(response => {
                    if(response.status === 200){
                        this.materia = response.data.materia
                        this.profesor = response.data.materia.profesor
                        this.examenes = response.data.materia.examenes.map(examen => {
                            //console.log(examen.fecha_aplicacion)
                            return {
                                ...examen,
                                fecha_before: moment(examen.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss'),
                                fecha_after: moment(examen.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss').add(moment(examen.tiempo_aplicacion, 'HH:mm:ss').minutes(), 'minutes').add(moment(examen.tiempo_aplicacion, 'HH:mm:ss').hours(), 'h'),
                                fecha_aplicacion: moment(examen.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss').format('LLLL'),
                                fecha_hasta: moment(examen.fecha_aplicacion, 'YYYY-MM-DD HH:mm:ss').add(moment(examen.tiempo_aplicacion, 'HH:mm:ss').minutes(), 'minutes').add(moment(examen.tiempo_aplicacion, 'HH:mm:ss').hours(), 'h').format('LLLL'),
                                tiempo_aplicacion: moment(examen.tiempo_aplicacion, 'HH:mm:ss').format('LT')
                            }
                        })
                    }else {
                        this.$message({
                            message: `Error: ${response.data.message}`,
                            type: 'error'
                        });
                    }
                    
                }).catch(err => {
                    console.log("err - " + err)
                    this.$message({
                        message: `Error al cargar las materias.`,
                        type: 'error'
                    });
                })
        }
    }
})