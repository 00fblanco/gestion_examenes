const controlador_alumno = new Vue({
    el: '#app',
    data(){
        return {
            materias: []
        }
    },
    created(){
        this.getMaterias();
    },
    methods: {
        inscribirAlumno(materia_id){
            
            axios.post('/materia/inscribir', {
                materia_id
            }).then(response => {
                if(response.status === 200){
                    this.$message({
                        message: `Solicitud enviada al profesor.`,
                        type: 'success'
                    });
                }else{
                    this.$message({
                        message: `Error: ${response.data.message}`,
                        type: 'error'
                    });
                }
            }).catch(err => {
                console.log(err)
                this.$message({
                    message: `Error mandar solicitud`,
                    type: 'error'
                });
            })
        },
        getMaterias(){
            axios.get('/materia')
            .then(response => {
                this.materias = response.data.materias.map(materia => {
                    return {
                        ...materia,
                        fecha_inicio: moment(materia.fecha_inicio).utc().format('YYYY-MM-DD'),
                        fecha_termino: moment(materia.fecha_termino).utc().format('YYYY-MM-DD')
                    }
                })
            }).catch(err => {
                console.log("err - " + err)
                this.$message({
                    message: `Error al cargar las materias.`,
                    type: 'error'
                });
            })
        }
    }
})